# i24news console streaming helper

This helps stream i24news video content without a GUI interface.  This assumes
a paid subscription.

# Install

1. Suggested to create a python3 virtualenv and to activate it.
2. Requires `mpv` video player separately installed.  If not accessible in the
path the script can be edited.
3. Install python libraries

        pip3 install -r requirements

4. Create a file `credentials.json` in the application directory with i24news
credentials.

        {
            "username": "me@example.com",
            "password": "i24news-password"
        }

# Running

Once the dependencies have been installed and the credential file has been
created, simply invoking the script as follows will present a list of video
categories.

    python3 i24newsStreamer.py [options]

For the most current options use `--help`.  To enable operational info messages
add the `-v` option.  (Enabling info level messaging has a large effect on the
output during authentication where it will print phase descriptions rather than
dots.) For verbose messages, mostly useful for debugging, use `-vv`.  To
specify a different file for credentials, use the `--credentials` option.

On starting the script, it will attempt to authenticate with i24news.tv and its
service provider cleeng.  Actions that are typically performed by humans
typically are preceded by a 2 second delay which adds up to several seconds to
authenticate.  On success a number list of options will be presented.  Choose
an action by typing the number of the option desired and pressing enter.  The
first menu presented correspond to the primary sections of the i24news.tv site.
Subsequently *all* of the items in the category that are returned in the
initial query are presented.  The videos with a category are in fact grouped
into subcategories.  The subcategories could be used to present a smaller
number of options, but are not currently used.

When a video is selected the meta data is read and all sources are shown in a
list. The sources vary by type, version and use of TLS.  When a source is
selected, a subprocess of `mpv` will be started with the source URL.

The `application/x-mpegURL` type may come in various
[ext-x-versions](https://developer.apple.com/documentation/http_live_streaming/about_the_ext-x-version_tag).
Initial observations have shown inclusion of both version 4 and version 5.

The type `application/dash+xml` is dynamic bitrate stream.
[Wikipedia](https://en.wikipedia.org/wiki/Dynamic_Adaptive_Streaming_over_HTTP)
has some additional explanation.

# Troubleshooting

If there is an error installing pycrypto with an error reporting that the
header file `gmp.h` or `libgmp.*` cannot be found because the library is
installed under /usr/local or /opt/local, the environmental variables LDFLAGS
and CFLAGS can be defined before installing with pip to allow the library and
header to be found:

    export LDFLAGS="-L/usr/local/lib"
    export CFLAGS="-I/usr/local/include"

# Caveats

This implementation reimplements the Javascript authentication process, which
means it may be a bit fragile and need to be maintained if the service provider
changes the flow.  A more robust approach would be to use a Javascript engine,
which may be an option for a future improvement.

